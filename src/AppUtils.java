public final class AppUtils {
    private AppUtils() {
        // no-op
    }

    /**
     * Convert numeric string to integer without using Integer.parseInt() or Integer.valueOf() or any third party libs.
     * Assumptions:
     * - the passed string is not null and not empty
     * - it's a number without decimal points
     * - it can also be a negative number (if time permits, extra points for this)
     *
     * Hint: to convert char into int you can substract '0', e.g: int digit = c - '0';
     *
     * @param numberString Numeric string, e.g.: "12345"
     * @return int matching the number string
     */
    public static int stringToInt(final String numberString) {
        return 0;
    }
}
