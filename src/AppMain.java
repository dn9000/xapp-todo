import java.util.regex.Pattern;

public class AppMain {

    public static void main( String[] args ) throws Exception {

        // TODO: implement AppUtils.stringToInt to make it pass the following validations
        validate(AppUtils.stringToInt("12345"), 12345);
        validate(AppUtils.stringToInt("-12345"), -12345);
        validate(AppUtils.stringToInt("1"), 1);
        validate(AppUtils.stringToInt("-1"), -1);
        validate(AppUtils.stringToInt("0"), 0);


        // TODO: create regex that will pass the following validations,
        //  i.e. input starting with sophos and ending with 2020
        final String regex = "TODO";

        validateRegexMatches(regex, "sophos2020");
        validateRegexMatches(regex, "Sophos2020");
        validateRegexMatches(regex, "sophos-2020");
        validateRegexMatches(regex, "sophos.2020");
        validateRegexMatches(regex, "sophos 2020");
        validateRegexMatches(regex, "sophos324324ncdsnjcdsnc2020");
        validateRegexNotMatches(regex, "sophos20202");
        validateRegexNotMatches(regex, "abcsophos-2020");
        validateRegexNotMatches(regex, "1sophos2020");
        validateRegexNotMatches(regex, "-sophos2020");
        validateRegexNotMatches(regex, " sophos2020 ");


        System.out.println("Passed!");
    }

    private static void validateRegexMatches(final String regex, final String input) {
        if (!Pattern.compile(regex).matcher(input).matches()) {
            throw new RuntimeException(String.format("%s should match %s", regex, input));
        }
    }

    private static void validateRegexNotMatches(final String regex, final String input) {
        if (Pattern.compile(regex).matcher(input).matches()) {
            throw new RuntimeException(String.format("%s should not match %s", regex, input));
        }
    }

    private static void validate(int actual, int expected) {
        if (actual != expected) {
            throw new RuntimeException(String.format("Failed: %d != %d", actual, expected));
        }
    }
}
